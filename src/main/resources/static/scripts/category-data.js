$(function () {
    // invoke loadUserData
    loadCategoryData()
})

//event handler on button add new subcategory
$('#btnAddNew').on('click', function () {
    let addNew = $('#addNewCategoryModal')
    addNew.modal('show');
})

// load user data into datatable
const loadCategoryData = _ => {
    $('#category-responsive-data-table').DataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            search: "_INPUT_",
            searchPlaceholder: "Search...",
            processing: "<i class='mdi mdi-loading mdi-48px mdi-spin'></i>"
        },
        "ajax": {
            "url": "/blogs/categories",
            "type": "POST",
            "dataType": "json",
            "contentType": "application/json",
            "data": function (d) {
                console.log('data is'+d)
                return JSON.stringify(d);
            }
        },
        "columnDefs": [
            {
                "targets": 0,
                "data": "name",
                "visible": true,
                "render": function (data) {
                    return `${data}`
                }
            },
            {
                "targets": 1,
                "data": "status",
                "visible": true,
                "render": function (data) {
                    return `<span style="white-space: normal">${data}</span>`
                }
            },
            {
                "targets": 3,
                "data": "description"
            }
        ],
        "pagingType": "full_numbers",
        "pageLength": 10,
        "dom": '<"row justify-content-between top-information"lf>rt<"row justify-content-between bottom-information"ip><"clear">'
    });
}


