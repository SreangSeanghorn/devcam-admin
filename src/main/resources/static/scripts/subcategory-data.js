$(function () {
    // invoke loadUserData
    loadSubCategoryData()
})

//event handler on button add new subcategory
$('#btnAddNewSubcategory').on('click', function () {
    let addNewSubcategory = $('#addNewSubCategoryModal')
        addNewSubcategory.modal('show');
})

// load user data into datatable
const loadSubCategoryData = _ => {
    $('#subcategory-responsive-data-table').DataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            search: "_INPUT_",
            searchPlaceholder: "Search...",
            processing: "<i class='mdi mdi-loading mdi-48px mdi-spin'></i>"
        },
        "ajax": {
            "url": "/blogs/subcategories",
            "type": "POST",
            "dataType": "json",
            "contentType": "application/json",
            "data": function (d) {
                return JSON.stringify(d);
            }
        },
        "columnDefs": [
            {
                "targets": 0,
                "data": "name",
                "visible": true,
                "render": function (data) {
                    return `${data}`
                }
            },
            {
                "targets": 1,
                "data": "description",
                "visible": true,
                "render": function (data) {
                    return `<span style="white-space: normal">${data}</span>`
                }
            },
            {
                "targets": 2,
                "data": "categories_title"
            },
            {
                "targets": 3,
                "data": "status"
            }
        ],
        "pagingType": "full_numbers",
        "pageLength": 10,
        "dom": '<"row justify-content-between top-information"lf>rt<"row justify-content-between bottom-information"ip><"clear">'
    });
}


