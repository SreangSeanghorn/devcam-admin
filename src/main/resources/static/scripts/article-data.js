$(function () {
    // invoke loadUserData
    loadArticleData()
})

//event handler on button add new article
$('#btnAddNewArticle').on('click', function () {
    let addNew = $('#addNewArticleModal')
    addNew.modal('show')
})

// load user data into datatable
const loadArticleData = _ => {
    $('#article-responsive-data-table').DataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            search: "_INPUT_",
            searchPlaceholder: "Search...",
            processing: "<i class='mdi mdi-loading mdi-48px mdi-spin'></i>"
        },
        "ajax": {
            "url": "/blogs/articles",
            "type": "POST",
            "dataType": "json",
            "contentType": "application/json",
            "data": function (d) {
                return JSON.stringify(d);
            }
        },
        "columnDefs": [
            {
                "targets": 0,
                "data": "title",
                "visible": true,
                "render": function (data) {
                    return `${data}`
                }
            },
            {
                "targets": 1,
                "data": "description",
                "visible": true,
                "render": function (data) {
                    return `<span style="white-space: normal">${data}</span>`
                }
            },
            {
                "targets": 2,
                "data": "created_by",
                "render": function (data) {
                    return `${data}`
                }
            },
            {
                "targets": 3,
                "data": "created_date",
                "render": function (data) {
                    return `${data}`
                }
            },
            {
                "targets": 4,
                "data": "updated_date",
                "render": function (data) {
                    return `${data}`
                }

            },
            {
                "targets": 5,
                "data": "updated_date",
                "render": function (data) {
                    return `${data}`
                }
            },
            {
                "targets": 6,
                "data": "status",
                "render": function (data) {
                    return `${data}`
                }
            }
        ],
        "pagingType": "full_numbers",
        "pageLength": 10,
        "dom": '<"row justify-content-between top-information"lf>rt<"row justify-content-between bottom-information"ip><"clear">'
    });
}


