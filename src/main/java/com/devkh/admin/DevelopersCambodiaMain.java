package com.devkh.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevelopersCambodiaMain {

    public static void main(String[] args) {
        SpringApplication.run(DevelopersCambodiaMain.class, args);
    }

}
