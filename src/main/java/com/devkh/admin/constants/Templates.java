package com.devkh.admin.constants;

public class Templates {
    
    public final static String LOGIN = "auth/login";

    public final static String DASHBOARD = "dashboard/index";

    public final static String CUSTOMER_CREATE_VIEW = "customer/createView";

}
