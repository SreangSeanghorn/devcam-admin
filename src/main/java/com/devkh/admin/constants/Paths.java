package com.devkh.admin.constants;

public class Paths {
    
    public final static String INDEX = "/";

    public final static String LOGIN = "/login";

    public final static String CREATE_CUSTOMER = "/create-customer";

}
