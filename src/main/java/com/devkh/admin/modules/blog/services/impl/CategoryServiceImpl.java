package com.devkh.admin.modules.blog.services.impl;

import com.devkh.admin.modules.blog.models.Categories.CategoryComparators;
import com.devkh.admin.modules.blog.models.Categories.CategoryResponseModel;
import com.devkh.admin.modules.blog.models.datatable.Column;
import com.devkh.admin.modules.blog.models.datatable.Order;
import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.models.request.CategoryRequestModel;
import com.devkh.admin.modules.blog.repositories.CategoryRepository;
import com.devkh.admin.modules.blog.services.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    private static final Comparator<CategoryResponseModel> EMPTY_COMPARATOR = (e1, e2) -> 0;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }
    @Override
    public Page<CategoryResponseModel> getCategory(PagingRequest pagingRequest) {
        System.out.println("paging request == " + pagingRequest);
        try {
            List<CategoryResponseModel> categories = categoryRepository.select(pagingRequest);
            log.info("categories == " + categories);
            log.info("categories count == " + categories.size());
            return getPage(categories, pagingRequest);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        return new Page<>();
    }
    private Page<CategoryResponseModel> getPage(List<CategoryResponseModel> categories, PagingRequest pagingRequest) {

        List<CategoryResponseModel> filtered = categories.stream()
                .sorted(sortCategories(pagingRequest))
                .filter(filterCategories(pagingRequest))
                .collect(Collectors.toList());

        long count = categoryRepository.count();

        Page<CategoryResponseModel> page = new Page<>(filtered);
        page.setRecordsFiltered((int) count);
        page.setRecordsTotal((int) count);
        page.setDraw(pagingRequest.getDraw());

        return page;

    }



    private Predicate<? super CategoryResponseModel> filterCategories(PagingRequest pagingRequest) {
        if (pagingRequest.getSearch() == null || StringUtils.isEmpty(pagingRequest.getSearch().getValue())) {
            return category -> true;
        }

        String value = pagingRequest.getSearch()
                .getValue();
        System.out.println(value);
        return category -> category.getId().toString() == value
                || category.getName()
                .toLowerCase()
                .contains(value);
    }



    private Comparator<? super CategoryResponseModel> sortCategories(PagingRequest pagingRequest) {

        if (pagingRequest.getOrder() == null) {
            return EMPTY_COMPARATOR;
        }

        try {
            Order order = pagingRequest.getOrder()
                    .get(0);

            int columnIndex = order.getColumn();
            Column column = pagingRequest.getColumns()
                    .get(columnIndex);

            Comparator<CategoryResponseModel> comparator = CategoryComparators.getComparator(column.getData(), order.getDir());
            if (comparator == null) {
                return EMPTY_COMPARATOR;
            }

            return comparator;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return EMPTY_COMPARATOR;

    }


    @Override
    public List<CategoryResponseModel> selectAllCategories() {
        List<CategoryResponseModel> responseModels = new ArrayList<>();
        responseModels = categoryRepository.selectAllCategories();
        return responseModels;
    }

    @Override
    public Boolean insert(CategoryRequestModel categoryRequestModel) {
        System.out.println("category: "+categoryRequestModel);
        System.out.println(categoryRepository.insert(categoryRequestModel));
        return categoryRepository.insert(categoryRequestModel);
    }


}
