package com.devkh.admin.modules.blog.repositories;

import java.util.List;

import com.devkh.admin.modules.blog.models.datatable.PagingRequest;

import com.devkh.admin.modules.blog.responses.ArticleResponse;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository  {
    
    @Select("SELECT ar.id,title,description,u.username as created_by, to_char(created, 'DD Mon YYYY') as created_date,to_char(updated, 'DD Mon YYYY') as updated_date,ar.status FROM articles ar JOIN users u on ar.user_id=u.id OFFSET #{pagingRequest.start} LIMIT #{pagingRequest.length}")
    List<ArticleResponse> select(@Param("pagingRequest") PagingRequest pagingRequest);

    @Select("SELECT COUNT(*) FROM articles")
    int count();

    @Select("SELECT title,description,u.username as created_by, to_char(created, 'DD Mon YYYY') as created_date,to_char(updated, 'DD Mon YYYY') as updated_date,ar.status FROM articles ar JOIN users u on ar.user_id=u.id ")
    List<ArticleResponse> selectAllArticle();

}
