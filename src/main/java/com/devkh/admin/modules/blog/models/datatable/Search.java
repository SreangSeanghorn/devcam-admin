package com.devkh.admin.modules.blog.models.datatable;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class Search {
    
    private String value;
    private String regexp;

}
