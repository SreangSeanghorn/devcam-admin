package com.devkh.admin.modules.blog.services;

import com.devkh.admin.modules.blog.models.Categories.CategoryResponseModel;
import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.models.request.CategoryRequestModel;

import java.util.List;

public interface CategoryService {

    Page<CategoryResponseModel> getCategory(PagingRequest pagingRequest);
    List<CategoryResponseModel> selectAllCategories();
    Boolean insert(CategoryRequestModel categoryRequestModel);

}
