package com.devkh.admin.modules.blog.controllers.web;

import com.devkh.admin.modules.blog.models.request.CategoryRequestModel;
import com.devkh.admin.modules.blog.services.impl.ArticleServiceImpl;
import com.devkh.admin.modules.blog.utils.Paths;
import com.devkh.admin.modules.blog.utils.Views;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(Paths.BLOG)
public class ArticleController {

    private final ArticleServiceImpl articleServiceImp;

    @Autowired
    public ArticleController(ArticleServiceImpl articleServiceImp) {
        this.articleServiceImp = articleServiceImp;
    }


    @GetMapping(Paths.ARTICLE_DATA)
    public String requestArticleDataView() {
        return Views.BLOG_ARTICLE_DATA;
    }


}
