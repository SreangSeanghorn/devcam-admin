package com.devkh.admin.modules.blog.controllers.rest;

import com.devkh.admin.modules.blog.models.Categories.CategoryResponseModel;
import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.models.request.CategoryRequestModel;
import com.devkh.admin.modules.blog.services.impl.CategoryServiceImpl;
import com.devkh.admin.modules.blog.utils.Paths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Paths.BLOGS+Paths.CATEGORIES)
public class CategoryRestController {
    private final CategoryServiceImpl categoryService;
    @Autowired
    public CategoryRestController(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping
    public Page<CategoryResponseModel> list(@RequestBody PagingRequest pagingRequest){
        return categoryService.getCategory(pagingRequest);
    }

    @PostMapping("/add")
    public Boolean createCategory(@RequestBody CategoryRequestModel categoryRequestModel){
        return categoryService.insert(categoryRequestModel);
    }
}
