package com.devkh.admin.modules.blog.models.datatable;

import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Order {
    
    private Integer column;
    private Direction dir;

}
