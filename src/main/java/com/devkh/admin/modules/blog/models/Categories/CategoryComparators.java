package com.devkh.admin.modules.blog.models.Categories;

import com.devkh.admin.modules.blog.models.datatable.Direction;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class CategoryComparators {
    @EqualsAndHashCode
    @AllArgsConstructor
    @Getter
    static class Key {
        String name;
        Direction dir;

    }

    static Map<CategoryComparators.Key, Comparator<CategoryResponseModel>> map = new HashMap<>();

    static {
        map.put(new CategoryComparators.Key("id", Direction.asc), Comparator.comparing(CategoryResponseModel::getId));
        map.put(new CategoryComparators.Key("id", Direction.desc), Comparator.comparing(CategoryResponseModel::getId)
                .reversed());

        map.put(new CategoryComparators.Key("name", Direction.asc), Comparator.comparing(CategoryResponseModel::getName));
        map.put(new CategoryComparators.Key("name", Direction.desc), Comparator.comparing(CategoryResponseModel::getName)
                .reversed());
    }

    public static Comparator<CategoryResponseModel> getComparator(String name, Direction dir) {
        return map.get(new CategoryComparators.Key(name, dir));
    }

    private CategoryComparators() { }

}

