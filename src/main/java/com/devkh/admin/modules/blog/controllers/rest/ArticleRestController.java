package com.devkh.admin.modules.blog.controllers.rest;

import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.responses.ArticleResponse;
import com.devkh.admin.modules.blog.services.impl.ArticleServiceImpl;
import com.devkh.admin.modules.blog.utils.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Paths.BLOGS + Paths.ARTICLES)
public class ArticleRestController {

    private final ArticleServiceImpl articleServiceImp;

    @Autowired
    public ArticleRestController(ArticleServiceImpl articleServiceImp) {
        this.articleServiceImp = articleServiceImp;
    }

    @PostMapping
    public Page<ArticleResponse> list(@RequestBody PagingRequest pagingRequest) {
        System.out.println("page in post(article): "+pagingRequest);
        return articleServiceImp.getArticles(pagingRequest);
    }


}
