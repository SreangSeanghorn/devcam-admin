package com.devkh.admin.modules.blog.models.Articles;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import com.devkh.admin.modules.blog.models.datatable.Direction;

import com.devkh.admin.modules.blog.responses.ArticleResponse;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

public final class ArticleComparators {

    @EqualsAndHashCode
    @AllArgsConstructor
    @Getter
    static class Key {
        String name;
        Direction dir;
    }

    static Map<Key, Comparator<ArticleResponse>> map = new HashMap<>();

    static {
        map.put(new Key("id", Direction.asc), Comparator.comparing(ArticleResponse::getId));
        map.put(new Key("id", Direction.desc), Comparator.comparing(ArticleResponse::getId)
                                                           .reversed());

        map.put(new Key("title", Direction.asc), Comparator.comparing(ArticleResponse::getTitle));
        map.put(new Key("title", Direction.desc), Comparator.comparing(ArticleResponse::getTitle)
                                                           .reversed());
    }

    public static Comparator<ArticleResponse> getComparator(String name, Direction dir) {
        return map.get(new Key(name, dir));
    }

    private ArticleComparators() { }

}
