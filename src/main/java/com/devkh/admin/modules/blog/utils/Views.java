package com.devkh.admin.modules.blog.utils;

public class Views {
    

    public final static String BLOG_ARTICLE_DATA = "blog/article-data";
    public final static String BLOG_CATEGORY_DATA = "blog/category-data";
    public final static String BLOG_SUBCATEGORY_DATA = "blog/subcategory-data";

}
