package com.devkh.admin.modules.blog.models.Subcategories;

import com.devkh.admin.modules.blog.models.SubCategoryResponse;
import com.devkh.admin.modules.blog.models.datatable.Direction;
import com.devkh.admin.modules.blog.responses.ArticleResponse;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class SubcategoryComparators {

    @EqualsAndHashCode
    @AllArgsConstructor
    @Getter
    static class Key {
        String name;
        Direction dir;
    }

    static Map<Key, Comparator<SubCategoryResponse>> map = new HashMap<>();

    static {
        map.put(new Key("id", Direction.asc), Comparator.comparing(SubCategoryResponse::getId));
        map.put(new Key("id", Direction.desc), Comparator.comparing(SubCategoryResponse::getId)
                .reversed());

        map.put(new Key("title", Direction.asc), Comparator.comparing(SubCategoryResponse::getName));
        map.put(new Key("title", Direction.desc), Comparator.comparing(SubCategoryResponse::getName)
                .reversed());
    }

    public static Comparator<SubCategoryResponse> getComparator(String name, Direction dir) {
        return map.get(new Key(name, dir));
    }

    private SubcategoryComparators() { }
}
