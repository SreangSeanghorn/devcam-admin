package com.devkh.admin.modules.blog.controllers.web;

import com.devkh.admin.modules.blog.models.Categories.CategoryResponseModel;
import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.models.request.CategoryRequestModel;
import com.devkh.admin.modules.blog.services.impl.CategoryServiceImpl;
import com.devkh.admin.modules.blog.utils.Paths;
import com.devkh.admin.modules.blog.utils.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(Paths.BLOG)
public class CategoryController {

    private final CategoryServiceImpl categoryService;

    @Autowired
    public CategoryController(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping(Paths.CATEGORY_DATA)
    public String requestCategoryDataView(@ModelAttribute CategoryRequestModel categoryRequestModel,ModelMap modelMap) {
        modelMap.addAttribute("category",categoryRequestModel);
        System.out.println("category: "+categoryRequestModel);
        return Views.BLOG_CATEGORY_DATA;
    }

}
