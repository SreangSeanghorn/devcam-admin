package com.devkh.admin.modules.blog.models.request;

import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CategoryRequestModel {
    private String name;
    private String description;
}
