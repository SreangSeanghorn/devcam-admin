package com.devkh.admin.modules.blog.controllers.web;

import com.devkh.admin.modules.blog.models.SubCategoryResponse;
import com.devkh.admin.modules.blog.services.impl.SubcategoryServiceImp;
import com.devkh.admin.modules.blog.utils.Paths;
import com.devkh.admin.modules.blog.utils.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(Paths.BLOG)
public class SubcategoryController {

    private final SubcategoryServiceImp subcategoryServiceImp;
    @Autowired
    public SubcategoryController(SubcategoryServiceImp subcategoryServiceImp) {
        this.subcategoryServiceImp = subcategoryServiceImp;
    }

    @GetMapping(Paths.SUB_CATEGORY_DATA)
    public String viewSubcategories(){
        return Views.BLOG_SUBCATEGORY_DATA;
    }
}
