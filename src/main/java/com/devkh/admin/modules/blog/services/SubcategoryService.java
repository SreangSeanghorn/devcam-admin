package com.devkh.admin.modules.blog.services;

import com.devkh.admin.modules.blog.models.SubCategoryResponse;
import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;

import java.util.List;

public interface SubcategoryService {

    List<SubCategoryResponse> selectAllSubcategories();
    Page<SubCategoryResponse> getSubcategories(PagingRequest pagingRequest);
    List<String> selectAllCategoryName();
}
