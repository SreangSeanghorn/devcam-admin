package com.devkh.admin.modules.blog.repositories;

import com.devkh.admin.modules.blog.models.SubCategoryResponse;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SubcategoryRepository {
    @Select("SELECT s.id,s.name,s.description,c.name as categories,s.status FROM subcategories s JOIN categories c on s.categoryid=c.id")
    @Results(id = "subcategory",value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "categories", property = "categories_title"),
            @Result(column = "status", property = "status")
    })
    List<SubCategoryResponse> selectAllSubcategories();

    @Select("SELECT COUNT(*) FROM subcategories")
    int count();

    @Select("SELECT s.id,s.name,s.description,c.name as categories,s.status FROM subcategories s JOIN categories c on s.categoryid=c.id OFFSET #{pagingRequest.start} LIMIT #{pagingRequest.length}")
    @Results(id = "subcategories",value = {
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "categories", property = "categories_title"),
            @Result(column = "status", property = "status")
    })
    List<SubCategoryResponse> select(@Param("pagingRequest") PagingRequest pagingRequest);

    @Select("SELECT DISTINCT c.name from subcategories s JOIN categories c on s.categoryid=c.id")
    List<String> selectCategoryName();
}
