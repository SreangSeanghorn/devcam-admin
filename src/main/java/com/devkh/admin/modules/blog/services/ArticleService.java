package com.devkh.admin.modules.blog.services;

import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.responses.ArticleResponse;

import java.util.List;

public interface ArticleService {
    
    Page<ArticleResponse> getArticles(PagingRequest pagingRequest);
    List<ArticleResponse> getAllArticles();
}
