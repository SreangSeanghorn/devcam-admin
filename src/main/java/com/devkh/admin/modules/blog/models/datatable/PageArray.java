package com.devkh.admin.modules.blog.models.datatable;

import java.util.List;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PageArray {
    
    private List<List<String>> data;
    private Integer recordsFiltered;
    private Integer recordsTotal;
    private Integer draw;

}
