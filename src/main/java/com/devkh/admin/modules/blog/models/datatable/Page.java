package com.devkh.admin.modules.blog.models.datatable;

import java.util.List;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class Page<T> {
    
    public Page(List<T> data) {
        this.data = data;
    }

    private List<T> data;
    private Integer recordsFiltered;
    private Integer recordsTotal;
    private Integer draw;

}
