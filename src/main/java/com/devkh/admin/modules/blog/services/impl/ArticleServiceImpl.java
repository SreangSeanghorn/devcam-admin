package com.devkh.admin.modules.blog.services.impl;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.devkh.admin.modules.blog.models.Articles.ArticleComparators;
import com.devkh.admin.modules.blog.models.datatable.Column;
import com.devkh.admin.modules.blog.models.datatable.Order;
import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.repositories.ArticleRepository;
import com.devkh.admin.modules.blog.responses.ArticleResponse;
import com.devkh.admin.modules.blog.services.ArticleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ArticleServiceImpl implements ArticleService {

    private ArticleRepository articleRepository;

    private static final Comparator<ArticleResponse> EMPTY_COMPARATOR = (e1, e2) -> 0;

    @Autowired
    public ArticleServiceImpl(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public Page<ArticleResponse> getArticles(PagingRequest pagingRequest) {
        System.out.println("paging request == " + pagingRequest);
        try {

            List<ArticleResponse> articles = articleRepository.select(pagingRequest);
          //  log.info("articles == " + articles);
            log.info("articles count == " + articles.size());
            return getPage(articles, pagingRequest);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        return new Page<>();
    }

    @Override
    public List<ArticleResponse> getAllArticles() {
        List<ArticleResponse> articles= articleRepository.selectAllArticle();
        System.out.println(articles.get(0));
        return articles;
    }


    private Page<ArticleResponse> getPage(List<ArticleResponse> articles, PagingRequest pagingRequest) {

        List<ArticleResponse> filtered = articles.stream()
                                         .sorted(sortArticles(pagingRequest))
                                         .filter(filterArticles(pagingRequest))
                                         .collect(Collectors.toList());
        long count = articleRepository.count();

        Page<ArticleResponse> page = new Page<>(filtered);
        page.setRecordsFiltered((int) count);
        page.setRecordsTotal((int) count);
        page.setDraw(pagingRequest.getDraw());

        return page;

    }

    private Predicate<ArticleResponse> filterArticles(PagingRequest pagingRequest) {

        if (pagingRequest.getSearch() == null || StringUtils.isEmpty(pagingRequest.getSearch().getValue())) {
            System.out.println("articles is null");
            return article -> true;
        }

        String value = pagingRequest.getSearch()
                                    .getValue();
        System.out.println("article search is: "+pagingRequest.getSearch().getValue());

        return article -> article.getId().toString() == value
                || article.getTitle()
                           .toLowerCase()
                           .contains(value);
    }



    private Comparator<ArticleResponse> sortArticles(PagingRequest pagingRequest) {

        if (pagingRequest.getOrder() == null) {
            return EMPTY_COMPARATOR;
        }

        try {
            Order order = pagingRequest.getOrder()
                                       .get(0);

            int columnIndex = order.getColumn();
            Column column = pagingRequest.getColumns()
                                         .get(columnIndex);

            Comparator<ArticleResponse> comparator = ArticleComparators.getComparator(column.getData(), order.getDir());
            if (comparator == null) {
                return EMPTY_COMPARATOR;
            }

            return comparator;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return EMPTY_COMPARATOR;

    }

}
