package com.devkh.admin.modules.blog.models.datatable;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PagingRequest {
    
    private Integer start;
    private Integer length;
    private Integer draw;
    private List<Order> order;
    private List<Column> columns;
    private Search search;


}
