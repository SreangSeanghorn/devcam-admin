package com.devkh.admin.modules.blog.services.impl;

import com.devkh.admin.modules.blog.models.SubCategoryResponse;
import com.devkh.admin.modules.blog.models.Subcategories.SubcategoryComparators;
import com.devkh.admin.modules.blog.models.datatable.Column;
import com.devkh.admin.modules.blog.models.datatable.Order;
import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.repositories.SubcategoryRepository;
import com.devkh.admin.modules.blog.services.SubcategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SubcategoryServiceImp implements SubcategoryService {

    private final SubcategoryRepository subcategoryRepository;

    private static final Comparator<SubCategoryResponse> EMPTY_COMPARATOR = (e1, e2) -> 0;

    @Autowired
    public SubcategoryServiceImp(SubcategoryRepository subcategoryRepository) {
        this.subcategoryRepository = subcategoryRepository;
    }

    @Override
    public List<SubCategoryResponse> selectAllSubcategories() {
        List<SubCategoryResponse> subCategoryResponses = new ArrayList<>();
        subCategoryResponses = subcategoryRepository.selectAllSubcategories();
        return subCategoryResponses;
    }

    @Override
    public Page<SubCategoryResponse> getSubcategories(PagingRequest pagingRequest) {
        System.out.println("paging request == " + pagingRequest);
        try {

            List<SubCategoryResponse> subCategoryResponseList = subcategoryRepository.select(pagingRequest);
            log.info("articles == " + subCategoryResponseList);
            log.info("sub count == " + subCategoryResponseList.size());
            return getPage(subCategoryResponseList, pagingRequest);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        return new Page<>();
    }

    @Override
    public List<String> selectAllCategoryName() {
        return subcategoryRepository.selectCategoryName();
    }


    private Page<SubCategoryResponse> getPage(List<SubCategoryResponse> subcategory, PagingRequest pagingRequest) {
        List<SubCategoryResponse> filtered = subcategory.stream()
                .sorted(sortSubcategory(pagingRequest))
                .filter(filterSubcategory(pagingRequest))
                .collect(Collectors.toList());
        long count = subcategoryRepository.count();

        Page<SubCategoryResponse> page = new Page<>(filtered);
        page.setRecordsFiltered((int) count);
        page.setRecordsTotal((int) count);
        page.setDraw(pagingRequest.getDraw());

        return page;

    }



    private Predicate<SubCategoryResponse> filterSubcategory(PagingRequest pagingRequest) {
        if (pagingRequest.getSearch() == null || StringUtils.isEmpty(pagingRequest.getSearch().getValue())) {
            System.out.println("sub is null");
            return subcategory -> true;
        }

        String value = pagingRequest.getSearch()
                .getValue();
        System.out.println("sub search is: "+value);
        return subcategory -> subcategory.getId().toString() == value
                || subcategory.getName()
                .toLowerCase()
                .contains(value);
    }



    private Comparator<SubCategoryResponse> sortSubcategory(PagingRequest pagingRequest) {

        if (pagingRequest.getOrder() == null) {
            return EMPTY_COMPARATOR;
        }

        try {
            Order order = pagingRequest.getOrder()
                    .get(0);

            int columnIndex = order.getColumn();
            Column column = pagingRequest.getColumns()
                    .get(columnIndex);

            Comparator<SubCategoryResponse> comparator = SubcategoryComparators.getComparator(column.getData(), order.getDir());
            if (comparator == null) {
                return EMPTY_COMPARATOR;
            }

            return comparator;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return EMPTY_COMPARATOR;

    }
}
