package com.devkh.admin.modules.blog.utils;

public class Paths {


    public final static String BLOG = "/blog";
    
    public final static String ARTICLE_DATA = "/article-data";
    public final static String CATEGORY_DATA = "/category-data";
    public final static String SUB_CATEGORY_DATA = "/subcategory-data";


    // REST CONSTANTS
    public final static String BLOGS = "/blogs";
    public final static String ARTICLES = "/articles";
    public final static String CATEGORIES = "/categories";
    public final static String SUBCATEGORIES = "/subcategories";

    
}
