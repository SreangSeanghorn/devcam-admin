package com.devkh.admin.modules.blog.models.datatable;

public enum Direction {
    
    asc,
    desc;

}
