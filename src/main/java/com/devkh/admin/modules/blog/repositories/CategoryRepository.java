package com.devkh.admin.modules.blog.repositories;

import com.devkh.admin.modules.blog.models.Categories.CategoryResponseModel;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.models.request.CategoryRequestModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("SELECT id,name,status,description FROM categories OFFSET #{pagingRequest.start} LIMIT #{pagingRequest.length}")
    List<CategoryResponseModel> select(@Param("pagingRequest") PagingRequest pagingRequest);

    @Select("SELECT COUNT(*) FROM categories")
    int count();

    @Select("SELECT name,status,description FROM categories")
    List<CategoryResponseModel> selectAllCategories();

    @Insert("INSERT INTO categories(name,description) VALUES(#{categoryRequestModel.name},#{categoryRequestModel.description})")
    Boolean insert(@Param("categoryRequestModel") CategoryRequestModel categoryRequestModel);


}
