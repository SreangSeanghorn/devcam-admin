package com.devkh.admin.modules.blog.controllers.rest;

import com.devkh.admin.modules.blog.models.SubCategoryResponse;
import com.devkh.admin.modules.blog.models.datatable.Page;
import com.devkh.admin.modules.blog.models.datatable.PagingRequest;
import com.devkh.admin.modules.blog.models.request.CategoryRequestModel;
import com.devkh.admin.modules.blog.services.impl.SubcategoryServiceImp;
import com.devkh.admin.modules.blog.utils.Paths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(Paths.BLOGS+Paths.SUBCATEGORIES)
public class SubcategoryRestController {

    private final SubcategoryServiceImp subcategoryServiceImp;

    @Autowired
    public SubcategoryRestController(SubcategoryServiceImp subcategoryServiceImp) {
        this.subcategoryServiceImp = subcategoryServiceImp;
    }
    @PostMapping
    public Page<SubCategoryResponse> list(@RequestBody PagingRequest pagingRequest) {
        System.out.println("sub categories post : "+pagingRequest);
        System.out.println("subcate items: "+subcategoryServiceImp.getSubcategories(pagingRequest));
        return subcategoryServiceImp.getSubcategories(pagingRequest);
    }
    @GetMapping("/category")
    public List<String> selectAllCategoryName(){
        System.out.println("Sub Category:"+subcategoryServiceImp.selectAllCategoryName());
        subcategoryServiceImp.selectAllCategoryName();
        return  subcategoryServiceImp.selectAllCategoryName();
    }
}
