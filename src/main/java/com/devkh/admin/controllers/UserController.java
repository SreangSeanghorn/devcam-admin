package com.devkh.admin.controllers;

import com.devkh.admin.constants.Paths;
import com.devkh.admin.constants.Templates;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
    
    @GetMapping(Paths.CREATE_CUSTOMER)
    public String viewCreateNewPage() {
        return Templates.CUSTOMER_CREATE_VIEW;
    }

}
