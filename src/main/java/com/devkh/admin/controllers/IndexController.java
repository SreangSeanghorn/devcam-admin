package com.devkh.admin.controllers;

import com.devkh.admin.constants.Paths;
import com.devkh.admin.constants.Templates;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping(Paths.INDEX)
    public String viewIndex() {
        return Templates.DASHBOARD;
    }

}
